﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace AdfsTest01.Controllers
{
    [RoutePrefix("api/v1")]
    public class DefaultApiController : ApiController
    {
        [Route("helloworld")]
        [HttpGet]
        public string helloWorld()
        {
            return "Hello World";
        }

        [Authorize(Roles = "Domain Users")]
        [RouteAttribute("secure/helloworld")]
        [HttpGet]
        public string helloWorldSecure()
        {
            return "Hello Secure World";
        }

        [Authorize(Roles = "IT_Managers")]
        [RouteAttribute("secure/helloworld/itall")]
        [HttpGet]
        public string helloWorldSecureFail()
        {
            IEnumerable<Claim> claims = ((System.Security.Claims.ClaimsIdentity)ClaimsPrincipal.Current.Identity).Claims;
            var displayName = claims.SingleOrDefault(c => c.Type == ClaimTypes.Name).Value;

            return "Hello " + displayName + " -- You are in the group IT_ALL";
        }
    }
}
