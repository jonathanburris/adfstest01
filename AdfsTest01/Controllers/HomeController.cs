﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AdfsTest01.Controllers
{
    public class HomeController : Controller
    {
        //[Authorize(Roles = "IT_Application Tech Leads")]
        public ActionResult Index()
        {
            
            IEnumerable<Claim> claims = ((System.Security.Claims.ClaimsIdentity)ClaimsPrincipal.Current.Identity).Claims;

            //var nameIdentifier = claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var userPrincipalName = claims.FirstOrDefault(c => c.Type == ClaimTypes.Upn).Value;
            var displayName = claims.SingleOrDefault(c => c.Type == ClaimTypes.Name).Value;
            var givenName = claims.SingleOrDefault(c => c.Type == ClaimTypes.GivenName).Value;
            var surname = claims.SingleOrDefault(c => c.Type == ClaimTypes.Surname).Value;
            var email = claims.SingleOrDefault(c => c.Type == ClaimTypes.Email).Value;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value);

            ViewBag.userPrincipalName = userPrincipalName;
            ViewBag.displayName = displayName;
            ViewBag.givenName = givenName;
            ViewBag.surname = surname;
            ViewBag.email = email;
            ViewBag.roles = roles;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}